from django.contrib import admin
from .models import Project


class ProjectAdmin(Project):
    pass


admin.site.register(Project)
