from .models import Project
from tasks.models import Task
from django.urls import reverse_lazy
from django.views.generic import ListView, DetailView, CreateView
from django.contrib.auth.mixins import LoginRequiredMixin


class ProjectListView(LoginRequiredMixin, ListView):
    model = Project
    template_name = "proj_temps/proj_list.html"
    context_object_name = "projectlistcontext"

    def get_queryset(self):
        return Project.objects.filter(members=self.request.user)


class ProjectDetail(LoginRequiredMixin, DetailView):
    model = Project
    template_name = "proj_temps/proj_detail.html"
    context_object_name = "projectdetail"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["project_task"] = Task.objects.all()
        return context


class ProjectCreate(LoginRequiredMixin, CreateView):
    model = Project
    fields = ["name", "description", "members"]
    template_name = "proj_temps/proj_create.html"
    context_object_name = "projectcreate"

    def get_success_url(self):
        return reverse_lazy("show_project", kwargs={"pk": self.object.pk})
