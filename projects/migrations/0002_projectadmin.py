# Generated by Django 4.0.6 on 2022-08-02 23:46

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ("projects", "0001_initial"),
    ]

    operations = [
        migrations.CreateModel(
            name="ProjectAdmin",
            fields=[
                (
                    "project_ptr",
                    models.OneToOneField(
                        auto_created=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        parent_link=True,
                        primary_key=True,
                        serialize=False,
                        to="projects.project",
                    ),
                ),
            ],
            bases=("projects.project",),
        ),
    ]
