from django.urls import path
from .views import ProjectListView, ProjectDetail, ProjectCreate

urlpatterns = [
    path("", ProjectListView.as_view(), name="list_projects"),
    path("<int:pk>/", ProjectDetail.as_view(), name="show_project"),
    path("create/", ProjectCreate.as_view(), name="create_project"),
]
