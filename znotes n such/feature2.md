---
Feature 2:
This Django application needs to have a Django project named tracker and three 
Django apps named accounts, projects, and tasks. Those three Django apps need 
to be properly installed in the Django project so it can find them.

In the repository directory:

[x] Create a Django project named tracker so that the manage.py file is in the top directory -- django-admin startproject «name» .
[x] Create a Django app named accounts and install it in the tracker Django project in the INSTALLED_APPS list -- python manage.py startapp «name»
[x] Create a Django app named projects and install it in the tracker Django project in the INSTALLED_APPS list -- python manage.py startapp «name»
[x] Create a Django app named tasks and install it in the tracker Django project in the INSTALLED_APPS list -- python manage.py startapp «name»
[x] Run the migrations -- python manage.py makemigrations - python manage.py migrate
[x] Create a super user -- python manage.py createsuperuser

Test:
python manage.py test tests.test_feature_02
---