---
Feature 13:
This feature allows people to see the details about a project.

[x] Create a view that shows the details of a particular project
[x] A user must be logged in to see the view
[x] In the projects urls.py file, register the view with the path "<int:pk>/" and the name "show_project"
[x] Create a template to show the details of the project and a table of its tasks
[x] Update the list template to show the number of tasks for a project
[x] Update the list template to have a link from the project name to the detail view for that project

HTML:
--There is one template to create and one to modify.--

Create:
The project detail view should render to HTML that has the following structure

the fundamental five 
    a main tag that contains
        div tag that contains
            an h1 tag with the project's name as its content
            a p tag with the project's description in it
            an h2 tag that has the content "Tasks"
if the project has tasks, then
            a table that contains five columns with the headers "Name", "Assignee", "Start date", "Due date", and "Is completed" with rows for each task in the project
otherwise
            a p tag with the content "This project has no tasks"
Note: the "Is completed" column shows "yes" and "no" for the task is_completed status.

Modify:
The project list view should now contain an a tag to the project detail page for the indicated project title in the table's first column.

The second column should now show the total number of tasks for a project.

Test:
python manage.py test tests.test_feature_13
---

Failing