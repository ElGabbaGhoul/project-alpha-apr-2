---
Feature 11:

The Task model should have the following attributes:
Name	        Type	    Constraints
[x] name	        string	    maximum length of 200 characters
[x] start_date	date-time	
[x] due_date	    date-time	
[x] is_completed	Boolean	    default value should be False
[x] project	        foreign key	Refers to the projects.Project model, related name "tasks", on  delete cascade
[x] assignee	    foreign key	Refers to the auth.User model, null is True, related name "tasks", on delete set null

Moreover the Task model should implicitly convert to a string with the __str__ method that is the value of the name property.

Test:
python manage.py test tests.test_feature_11

---