---
Feature 10:
You need to create a function view to handle showing the sign-up form, and handle its submission. This is a view, so you should create it in the file in the accounts directory that should hold the views.

[x] You'll need to import the UserCreationForm from the built-in auth forms 
[x] You'll need to use the special create_user  method to create a new user account from their username and password
[x] You'll need to use the login  function that logs an account in
[x] After you have created the user, redirect the browser to the path registered with the name "home"
[x] Create an HTML template named signup.html in the registration directory
[x] Put a post form in the signup.html and any other required HTML or template inheritance stuff that you need to make it a valid Web page with the fundamental five (see specifications below)

HTML:
The resulting HTML from a request to the path http://localhost:8000/accounts/signup/  should result in HTML that has

the fundamental five in it
a main tag that contains
div tag that contains
an h1 tag with the content "Signup"
a form tag with method "post" that contains any kind of HTML structures but must include
an input tag with type "text" and name "username"
an input tag with type "password" and name "password1"
an input tag with type "password" and name "password2"
a button with the content "Signup"

Test:
python manage.py test tests.test_feature_10
---
