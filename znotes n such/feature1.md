---
Feature 1:
[x] # Create a new virtual environment in the repository directory for the project -- python -m venv .venv
[x] # Activate the virtual environment -- ./.venv/Scripts/Activate.ps1
[x] # Upgrade pip -- python -m pip install --upgrade pip
[x] # Install Django -- pip install django
[x] # Install black -- pip install black
[x] # Install flake8 -- python -m pip install flake8
[x] # Install djhtml -- pip install djhtml
[x] # Deactivate your virtual environment -- deactivate 
[x] # Activate your virtual environment -- ./.venv/Scripts/Activate.ps1
[x] # Use pip freeze to generate a requirements.txt file -- python -m pip freeze > requirements.txt

Test:
python -m unittest tests.test_feature_01
[x] pass test
---