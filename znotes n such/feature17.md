---
Feature 17:

This feature allows a person to update the status on one of their assigned tasks from incomplete to complete.

[x] Create an update view for the Task model that only is concerned with the is_completed field
[x] When the view successfully handles a submission, it should redirect to the "show_my_tasks" URL path, that is, it should redirect to the "My Tasks" view (success_url property on a view class)
[x] Register that view in the tasks app for the path "<int:pk>/complete/" and the name "complete_task" in the tasks urls.py file
* You do not need to make a template for this view *
[] Modify the "My Tasks" view to comply with the template specification

HTML: 
In the table cell that holds the status for each task in the "My Tasks" view, if the value of is_completed is False, then show this form.

```<form method="post" action="{% url 'complete_task' task.id %}">
  {% csrf_token %}
  <input type="hidden" name="is_completed" value="True">
  <button>Complete</button>
</form>```

When the person clicks that button, the task's is_completed status should be updated to True and, then, the form disappears

Testing:
python manage.py test tests.test_feature_17

---