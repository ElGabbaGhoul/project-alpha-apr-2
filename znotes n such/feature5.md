---
Feature 5:

This feature is about creating a list view for the Project model, registering the view for a path, registering the projects paths with the tracker project, and creating a template for the view.

[x] Create a view that will get all of the instances of the Project model and puts them in the context for the template
[x] Register that view in the projects app for the path "" and the name "list_projects" in a new file named projects/urls.py
[x] Include the URL patterns from the projects app in the tracker project with the prefix "projects/"
[x] Create a template for the list view that complies with the following specifications

Template Specs:
The resulting HTML from a request to the path http://localhost:8000/projects/  should result in HTML that has

the fundamental five in it
a main tag that contains
    div tag that contains
        an h1 tag with the content "My Projects"
        if there are no projects created, then
            a p tag with the content "You are not assigned to any projects"
        otherwise
            a table that has two columns,
                The first with the header "Name" and the rows with the names of the projects
                The second with the header "Number of tasks" and nothing in those rows because we don't yet have tasks

Test:
python manage.py test tests.test_feature_05

---