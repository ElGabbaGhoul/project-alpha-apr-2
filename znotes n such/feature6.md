---
Feature 6:

This feature is about redirecting http://localhost:8000/  to the project list page created in Feature 5.

[] In the tracker urls.py, use the RedirectView to redirect from "" to the name of path for the list view that you created in the previous feature. Register that path a name of "home".


Test:
python manage.py test tests.test_feature_06

---