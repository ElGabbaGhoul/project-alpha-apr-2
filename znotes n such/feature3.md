---
Feature 3:
This feature is for you to create a Project model in the projects Django app.

The Project model should have the following attributes


NAME        TYPE            CONSTRAINTS
name	    string	        maximum length of 200 characters
description	string	        no maximum length
members	    many-to-many	Refers to the auth.User model, related name "projects"

Moreover the Project model should implicitly convert to a string with the __str__ method that is the value of the name property.

Make migrations and migrate your database.

Test:
python manage.py test tests.test_feature_03
---