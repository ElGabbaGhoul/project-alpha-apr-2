---
Feature 18:

In this, you will take the project description and make it Markdown enabled.

[x] Install the django-markdownify package using pip (the first pip command in the instructions) and put it into the INSTALLED_APPS in the tracker settings.py according to the Installation  instructions block

[x] Also, in the tracker settings.py file, add the configuration setting to disable sanitation 

[x] In your the template for the Project detail view, load the markdownify template library as shown in the Usage  section

[x] Replace the p tag and {{ project.description }} in the Project detail view with this code

{{ project.description|markdownify }}

[x] Use pip freeze to update your requirements.txt file > python -m pip freeze > requirements.txt <

Test:

> python manage.py test tests.test_feature_18 <

---