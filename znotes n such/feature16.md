---
Feature 16:

This feature allows a logged in person to see the tasks that are assigned to them.

[x] Create a list view for the Task model with the objects filtered so that the person only sees the tasks assigned to them by filtering with the assignee equal to the currently logged in user
[x] The view must only be accessible by people that are logged in
[x] Register that view in the tasks app for the path "mine/" and the name "show_my_tasks" in the tasks urls.py file
[x] Create an HTML template that conforms with the following specification

HTML:
The resulting HTML from a request to the path http://localhost:8000/tasks/mine/  should result in HTML that has

the fundamental five
a main tag that contains
    div tag that contains
        an h1 tag with the content "My Tasks"
        if there are tasks assigned to the person
            a table with the headers "Name", "Start date", "End date", "Is completed" and a row for each task that is assigned to the logged in person
            In the last column, if the task is completed, it should show the word "Done", otherwise, it should show nothing
        otherwise
            a p tag with the content "You have no tasks"

Testing:
python manage.py test tests.test_feature_16
---