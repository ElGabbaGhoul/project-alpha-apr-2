---
Feature 8:

This feature requires someone to login before accessing the list view for the projects. It also constrains the result set of projects to those that the person is a member of.

[x] Protect the list view for the Project model so that only a person that has logged in can access it
[] Change the queryset of the view to filter the Project objects where members equals the logged in user

Test:
python manage.py test tests.test_feature_08

failing
---