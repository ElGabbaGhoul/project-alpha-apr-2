---
Feature 15:

This feature is about creating a create view for the Task model, registering the view for a path, registering the tasks paths with the tracker project, and creating a template for the view.

[x] Create a view that will show a form to create an instance of the Task model for all properties except the is_completed field
[x] The view must only be accessible by people that are logged in
[x] When the view successfully handles the form submission, have it redirect to the detail page of the task's project
[x] Register that view in the tasks app for the path "create/" and the name "create_task" in a new file named tasks/urls.py
[x] Include the URL patterns from the tasks app in the tracker project with the prefix "tasks/"
[] Create a template for the create view that complies with the following specifications
[] Add a link to create a task from the project detail page that complies with the following specifications

HTML:
Two templates need either creation or modification.

Create:
The resulting HTML from a request to the path http://localhost:8000/tasks/create/  should result in HTML that has

the fundamental five
    a main tag that contains
        div tag that contains
            an h1 tag with the content "Create a new task"
            a form tag with method "post" that contains any kind of HTML structures but must include
                an input tag with type "text" and name "name"
                an input tag with type "text" and name "start_date"
                an input tag with type "text" and name "due_date"
                a select tag with name "projects"
                a select tag with name "assignee"
                a button tag with the content "Create"

Edit:
After the h2 in the project detail page, and before the table, add the following HTML

a p tag that contains
    an a tag with an href attribute that points to the "create_task" path with the content "Create a new task"

Test:
python manage.py test tests.test_feature_15
---

Failing