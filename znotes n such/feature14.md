---
Feature 14:
This feature allows a person to go from the project list page to a page that allows them to create a new project.

[x] Create a create view for the Project model that will show the name, description, and members properties in the form and handle the form submission to create a new Project
[x] A person must be logged in to see the view
[x] If the project is successfully created, it should redirect to the detail page for that project
[x] Register that view for the path "create/" in the projects urls.py and the name "create_project"
[x] Create an HTML template that shows the form to create a new Project (see the template specifications below)
[] Add a link to the list view for the Project that navigates to the new create view

HTML:
There are two templates that need to be changed or created.

The resulting HTML from a request to the path http://localhost:8000/projects/create/  should result in HTML that has

Create:
the fundamental five
    a main tag that contains
        div tag that contains
            an h1 tag with the content "Create a new project"
            a form tag with method "post" that contains any kind of HTML structures but must include
                an input tag with type "text" and name "name"
                a textarea tag with the name "description"
                a select tag with name "members"
                a button with the content "Create"

Change:
You need to add a link to the project list view after the h1 tag and before the table. The HTML that you add should be inside the div tag, after the h1 tag, and be

a p tag that contains
an a tag with an href attribute that points to the create project URL and has content "Create a new project"

Test:
python manage.py test tests.test_feature_14
---