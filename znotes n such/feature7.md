---
Feature 7:

This feature is about setting up a login page so that the person using the application can be identified.

[x] Make sure you have a super user created so you can login to test the feature.

[x] Register the LoginView in your accounts urls.py with the path "login/" and the name "login"
[x] Include the URL patterns from the accounts app in the tracker project with the prefix "accounts/"
[x] Create a templates directory under accounts
[x] Create a registration directory under templates
[x] Create an HTML template named login.html in the registration directory
[x] Put a post form in the login.html and any other required HTML or template inheritance stuff that you need to make it a valid Web page with the fundamental five (see specifications below)
[x] In the tracker settings.py file, create and set the variable LOGIN_REDIRECT_URL to the value "home" which will redirect us to the path (not yet created) with the name "home"

HTML:
The resulting HTML from a request to the path http://localhost:8000/accounts/login/  should result in HTML that has

the fundamental five
a main tag that contains
    div tag that contains
        an h1 tag with the content "Login"
        a form tag with method "post" that contains any kind of HTML structures but must include
        an input tag with type "text" and name "username"
        an input tag with type "password" and name "password"
        a button with the content "Login"

Test:
python manage.py test tests.test_feature_07

---
