---
Feature 19!:

In this feature, you'll add some navigation to the Web application. Hopefully, you have used template inheritance so that you can just do this in one place. Otherwise, you'll need to add the HTML to all of the places.

Template specification
On all HTML pages, add

a header tag as the first child of the body tag before the main tag that contains
    a nav tag that contains
        a ul tag that contains
        if the person is logged in,
            an li tag that contains
                an a tag with an href to the "show_my_tasks" path with the content "My tasks"
            an li tag that contains
                an a tag with an href to the "list_projects" path with the content "My projects"
            an li tag that contains
               an a tag with an href to the "logout" path with the content "Logout"
        otherwise
            an li tag that contains
                an a tag with an href to the "login" path with the content "Login"
            an li tag that contains
                an a tag with an href to the "signup" path with the content "Signup"

Testing:
> python manage.py test tests.test_feature_19 <

---