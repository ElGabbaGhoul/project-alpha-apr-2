---
Feature 9:

In the accounts/urls.py file,
[x] Import the LogoutView from the same module that you imported the LoginView from
[x] Register that view in your urlpatterns list with the path "logout/" and the name "logout"
[x] In the tracker settings.py file, create and set the variable LOGOUT_REDIRECT_URL to the value "login" which will redirect the logout view to the login page

Test:
python manage.py test tests.test_feature_09
---
