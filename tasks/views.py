from .models import Task
from django.urls import reverse, reverse_lazy
from django.views.generic import CreateView, UpdateView
from django.views.generic.list import ListView
from django.contrib.auth.mixins import LoginRequiredMixin


class CreateTask(LoginRequiredMixin, CreateView):
    model = Task
    template_name = "task_temps/task_create.html"
    fields = ["name", "start_date", "due_date", "project", "assignee"]
    context_object_name = "taskcreate"

    def get_success_url(self):
        return reverse_lazy("show_project", kwargs={"pk": self.object.pk})


class TaskList(LoginRequiredMixin, ListView):
    model = Task
    template_name = "task_temps/task_list.html"
    context_object_name = "tasklist"

    def get_queryset(self):
        return Task.objects.filter(assignee=self.request.user)


class UpdateTask(UpdateView):
    model = Task
    fields = ["is_completed"]
    context_object_name = "updatetask"

    def get_success_url(self):
        return reverse("show_my_tasks")
