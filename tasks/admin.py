from django.contrib import admin
from .models import Task


class TaskAdmin(Task):
    pass


admin.site.register(Task)
